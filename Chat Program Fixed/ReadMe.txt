To start the client program type
	python narcanus.py client
to start the server program type
	python narcanus.py server

A username/password record has been added to version 2 of narcanus
You can create a record by "server setup"->"Create Record" option
The records created will be saved on a txt file called UserRecords.txt
in the same directory as narcanus2

The server does not save plaintext passwords in the file, instead
the server will save the plaintext username and password hash.
Furthermore only the record owner (the server user that created the record)
will be able to read the record. Presently anybody can overwrite the record.

The program uses a SHA256 to hash passwords and AES CFB mode to encrypt the UserRecord file
The password hashes is what is used as a seed to AES CFB mode encryption.

ATTENTION: Make sure you start the server program first before the client. also. currently
if the client's username is NOT in the server's records file, you will get an error or
the encryption will fail

To exit the program while chating type :exit:

