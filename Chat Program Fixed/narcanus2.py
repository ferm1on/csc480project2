#!/usr/bin/python
import sys, Queue, threading, getpass
from socket import *
from encryption2 import *

# Print Prompt: Function to write msg to screen
# Pre-Condition: username and msg is a string
# Post-Condition: a promt will appear on the shell as <username> msg
def prompt(username,msg) :
    sys.stdout.write('<' + username + '> ' + msg)
    sys.stdout.flush()

# Read STD IN: "Speak Function", reads a line from terminal and puts it in a queue so that
# the main thread can process for transmission
# Pre-Condition: q is a queue object, key is a 32 byte object
# Post-Condition: puts an array on q as ['s',bytes(encrypted msg)], s stands for speak
def speak(q,key):
    while True:
        msg = sys.stdin.readline()
        q.put(['s',encrypt(msg,key)])

# Read Socket: "Listen Function", reads data from  the wire and puts in it a queue so thatpython
# the main thread can process for display
# Pre-Condition: q is a queue object, s is a socket object and key is a 32 byte object
# Post-Condition: puts an array on q as ['l',decrypted msg]. l stands for listen. the msg is recieved from the
# the socket object s
def listen(q,s,key):
    while True:
        msg = s.recv(1024)
        q.put(['l',decrypt(msg,key)])

# To test Client decrpty the message from the others
# If not, connection aborad
def clienttest(socket,key):
	ciphertext = encrypt("Hello",key)
	socket.sendall(ciphertext)
	bouncemsg = socket.recv(1024) 
	plaintext = decrypt(bouncemsg, key)
	if plaintext != "Hello":
		print("Corrupt Connection, aborting...")
		choice = raw_input("Would you like to continue?")
		if choice == "yes"
			return
		else:
			sys.exit()
	else:
		print("Connection is good!")

#bounce back the decrpty msg from server 
def bounceback(socket,key):
	msg = socket.recv(1024)
	msg = decrypt(msg,key)
	ciphertext = encrypt(msg,key)
	socket.sendall(ciphertext)

# code courtesy of
# http://stackoverflow.com/questions/1761744/python-read-password-from-stdin
# Get Username and password from user
# Pre-Condition: None
# Post-Condition: Returns a username as string and 32 byte object (hashed password)
def login():
    user = raw_input('\tUsername: ')
    if not user:
        user = getpass.getuser()

    pprompt = lambda: (getpass.getpass('\tPassword: '), getpass.getpass('\tRetype password: '))

    p1, p2 = pprompt()
    while p1 != p2:
        print('\tPasswords do not match. Try again')
        p1, p2 = pprompt()

    return user, get_key(p1)

# Prints appropriate menu
# Pre-Condition: menu is 'top' or 'setup'
# Post-Condition: Prints the coresponding menu and returns option chosen
def menu (menu):
    done = False
    
    if menu == 'top':
        while not done:
            option = raw_input('\t1. Server Setup\n\t2. Start Server\n\t3. Exit Application\n\n\tEnter Option: ')
            option = int(option)
            if option <= 3:
                done = True
            else:
                print '\tInvalid Option\n'

    elif menu == 'setup':
        while not done:
            option = raw_input('\t1. Create User Records\n\t   (Current Records will be overwrriten)\n\t2. Add User\n\t3. Remove User\n\t4. Print Records\n\t5. Save and Go Back\n\n\tEnter Option: ')
            option = int(option)
            if option <= 5:
                done = True
            else:
                print '\tInvalid Option\n'
    return option

# Create UserRecords file
# Pre-Condition: user is the to be owner of UserRecords and password is a 32 bytes object
# Post-Condition: a file named UserRecords.txt is created on narcanus2's directory
# the file is encrypted using password as key
# the file will contain 1 entry user,password
def create_records(user,password):
    UserRecords = file('UserRecords.txt','w')
    owner = user + ',' + password
    UserRecords.writelines(encrypt(owner,password))
    UserRecords.close()

# Read User Password and Names from file
# Pre Condition: user is a string object, password is a 32 bytes object
# Post Condition: returns a dictionary, key = users, entry = #passwords
def read_records(user,password):
    try:
        UserRecords =  open('UserRecords.txt')
        records = UserRecords.read()
        records = decrypt(records,password)
        dictionary = {}
        RecordsLenth = len(records.split(','))
        records = records.split(',')
        if records[RecordsLenth-2] == user and password == records[RecordsLenth-1]:
            for i in range(0,RecordsLenth,2):
                dictionary[records[i]] = records[i+1]
            return dictionary
        else:
            print ('\tCould not read records')
            print ('\tServer username and/or password do not macth')
        
    except:
        print ('\tUser records does not exist.\n')


# Save User Records
# Pre-Condition: Dictionary is a python dictionary, and password is 32 bytes object
# Post-Condition: Overwrites old UserRecords with dictionary
def save_records(dictionary,password):
    try:
        UserRecords = open('UserRecords.txt', 'w')
        users = dictionary.keys()
        passwords = dictionary.values()
        NewRecords = ''
        RecordSize = len(users)
        for i in range(RecordSize-1):
            NewRecords = NewRecords + users[i] + ',' + passwords[i] + ','
        NewRecords = NewRecords + users[RecordSize-1] + ',' + passwords[RecordSize-1]
        UserRecords.write(encrypt(NewRecords,password))

    except:
        print ('\tUser records does not exist.\n')

# Prints the record's user names
def print_records (dictionary):
    if not dictionary:
        print ('\tUser records is empty.\n')
    else:
        users = dictionary.keys()
        for i in range(len(users)):
            print ('\t' + users[i])
        print (' ')


if __name__ == '__main__':

    mode = str(sys.argv[1])
	
    #client mode: sets the app in "caller" mode. the program will try to connect to remote host
    if mode == 'client':
        print (" client mode choose")
        serveraddress = raw_input('\tEnter IPv4 Server Address: ')
        username, password = login()
		
	#Creating Socket and settings connecting
        s = socket(AF_INET,SOCK_STREAM)
        s.connect((serveraddress,8011))
        print('\tConnected to ' + serveraddress)

        #Exchange user names with remote host
	s.sendall(username)
        usernameremote = s.recv(1024)
		
	#Message Authentication Code/ HandShake / Negotiation
	#in this section we send a random msg to the remote host as ask for it back
	#if it matches our original msg we know we have the same key otherwise we must disconnect
        #At the same time, we are reciving the remote MAC challange, decrypting it, encrypting it and sending
        #it back for checking.
	MAC_LOCAL = bytes(random.randrange(1,pow(2,128)))
	key = negotiate_key(s,'client',get_generator(password))
	s.sendall(encrypt(MAC_LOCAL,key))
	MAC_HOST = s.recv(1024)
	MAC_HOST = decrypt(MAC_HOST,key)
	s.sendall(encrypt(MAC_HOST,key))
	MAC_RESPONSE = decrypt(s.recv(1024),key)
        if MAC_RESPONSE != MAC_LOCAL:
                  print("\tBAD KEY NEGOTIATION... EXITING APPLICATION\n")
                  sys.exit()
        else:
                  print("\tNegotiation Successful")
	 clienttest(key,s)

		
        print('\n\t' + usernameremote + ' has joined the chat...\n')

        #Prepare Threads and Q
        #The program has a main Q where it read msg to send to the remote host and msg to print to screen
        q = Queue.Queue()

        #speaker thread used to read for command shell inputs
        speak_worker = threading.Thread(target=speak, args=(q,key))
        speak_worker.setDaemon(True)
        speak_worker.start()

        #listen thread used to recieve remote host msgs
        listen_worker = threading.Thread(target=listen, args=(q,s,key))
        listen_worker.setDaemon(True)
        listen_worker.start()

        #CHAT (main Chat loop)
        #Here the program will whatch the Q and if it finds an item will take the appropriate response
        #depending on the flag it enconters, if the program finds a 'l' flag it will print the msg on screen
        #If the program finds a 's' flag it will send the msg to the remote host
        #the program always whatches for the string :exit:\n, if it finds it, it exits the application
        while True:
            if not q.empty():
                data = q.get()
                if data[0] == 'l':
                    if data[1] == ':exit:\n':
                        print(' Remote Connection Closed')
                        q.task_done()
                        sys.exit()

                    else:
                        prompt(usernameremote,data[1])
                        q.task_done()

                elif data[0] == 's':
                    if decrypt(data[1],key) == ':exit:\n':
                        print (' Exiting Application')
                        s.sendall(data[1])
                        q.task_done()
                        sys.exit()

                    else:
                        s.sendall(data[1])
                        q.task_done()
                    

    # server mode: sets the app in "receiver" mode. wait for connection from remote host
    elif mode == 'server':
        print (' server mode choosen\n')
        username, password = login()
        records = {}
        records = read_records(username,password)

        # Server Pre Setup
        done = False
        print (' ')
        while not done:
            option = menu('top')
            if option == 1:
                done2 = False
                print (' ')
                while not done2:
                    option = menu('setup')
                    if option == 1:
                        print ('\tNew Records owned by ' + username + ' created\n')
                        create_records(username,password)
                        records = read_records(username,password)
                    elif option == 2:
                        print ('\tEnter username and password for the new user\n')
                        newuser, newpass = login()
                        if records == {}:
                            print ('\tUser records does not exist.\n')
                        else:
                            records[newuser] = newpass
                            print ('\tUser record added.\n')
                    elif option == 3:
                        print ('\tEnter username to be deleted')
                        removeuser = raw_input('\tUsername: ')
                        print (' ')
                        try:
                            del records[removeuser]
                        except:
                            print ('\tUser does not exists\n')
                    elif option == 4:
                        print_records(records)
                    elif option == 5:
                        if not records:
                            done2 = True
                        else:
                            save_records(records,password)
                            done2 = True
            elif option == 3:
                    sys.exit()
            elif option == 2:
                    done = True

	#Creating Socket and wait for connection from remote host
        print ('\tWainting For Connection...')
        s = socket(AF_INET,SOCK_STREAM)
        s.bind(("",8011))
        s.listen(5)
        c,a = s.accept()
        print ("\tReceived Connection From " + a[0])

        #Exchange user names with remote host and check if remote username exists on record
	usernameremote = c.recv(1024)
        c.sendall(username)
        try:
            records[usernameremote]
        except:
            print ('\tRemote user does not exist on records')
            print (' Exiting Application')
            sys.exit()

        #Message Authentication Code/ HandShake / Negotiation
        #in this section we send a random msg to the remote host as ask for it back
	#if it matches our original msg we know we have the same key otherwise we must disconnect
        #At the same time, we are reciving the remote MAC challange, decrypting it, encrypting it and sending
        #it back for checking.
	MAC_LOCAL = bytes(random.randrange(1,pow(2,128)))
	key = negotiate_key(c,'server',get_generator(records[usernameremote]))
	MAC_HOST = c.recv(1024)
	c.sendall(encrypt(MAC_LOCAL,key))
	MAC_HOST = decrypt(MAC_HOST,key)
	MAC_RESPONSE = decrypt(c.recv(1024),key)
	c.sendall(encrypt(MAC_HOST,key))
		
	if MAC_RESPONSE != MAC_LOCAL:
		print("\tBAD KEY NEGOTIATION... EXITING APPLICATION\n")
		sys.exit()
	else:
		print("\tNegotiation Successful")

	bounceback(c,key)	
	
        print('\n\t' + usernameremote + ' has joined the chat...\n')
    
        #Prepare Threads and Q
        #The program has a main Q where it read msg to send to the remote host and msg to print to screen
        q = Queue.Queue()

        #speaker thread used to read for command shell inputs
        speak_worker = threading.Thread(target=speak, args=(q,key))
        speak_worker.setDaemon(True)
        speak_worker.start()

        #listen thread used to recieve remote host msgs
        listen_worker = threading.Thread(target=listen, args=(q,c,key))
        listen_worker.setDaemon(True)
        listen_worker.start()

        # CHAT
        #Here the program will whatch the Q and if it finds an item will take the appropriate response
        #depending on the flag it enconters, if the program finds a 'l' flag it will print the msg on screen
        #If the program finds a 's' flag it will send the msg to the remote host
        #the program always whatches for the string :exit:\n, if it finds it, it exits the application
        while True:
            if not q.empty():
                data = q.get()
                if data[0] == 'l':
                    if data[1] == ':exit:\n':
                        print(' Remote Connection Closed')
                        q.task_done()
                        sys.exit()

                    else:
                        prompt(usernameremote,data[1])
                        q.task_done()

                elif data[0] == 's':
                    if decrypt(data[1],key) == ':exit:\n':
                        print (' Exiting Application')
                        c.sendall(data[1])
                        q.task_done()
                        sys.exit()

                    else:
                        c.sendall(data[1])
                        q.task_done()
    else:
        print (" Choose valid mode: 'client' or 'server'")
        sys.exit()
