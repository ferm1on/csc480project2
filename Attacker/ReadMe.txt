To get the attacker program to work you might have to change ports on
the server section of narcanus. Or, if you have 2 computers it will
without any changes.

Here is how to get the attacker to work without doing a MiM attack

load narcanus client on your machine,
load attacker on your machine
load server on another machine.
make sure the server the attacker program is connecting to has the
right address

the attacker program will pring the conversation between the 2 
computers on the screen and on a txt file called chat log

to do the MiM attack, just change iptable forward rule to forward
the trafict intended for the server to the attacker machine instead
you will need to change the nat rules on the iptable, if the TCP
packages you are forwarding do not have the right destination address
your machine will reject them.

same is true for the packets you are forwarding to the chat server