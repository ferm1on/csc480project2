import sys, Queue, threading, time, struct
from socket import *
from encryption import *

# Print Prompt: Function to write msg to screen
def prompt(username,msg,f) :
    sys.stdout.write('<' + username + '> ' + msg)
    sys.stdout.flush()
    f.write(time.strftime('%H:%M:%S') + ' <' + username + '> ' + msg + '\n')

# Read Socket: "Listen Function", reads data from  the wire and puts in it a queue so thatpython
# the main thread can process for display
def listen(q,s,who,key):
    while True:
        try:
            msg = s.recv(1024)
            q.put([who,decrypt(msg,key)])
        except:
            pass
        

if __name__ == '__main__':

    #Creating log file to to save conversation
    LogFile = file('chatlog.txt','a')

    #Creating Socket to connect to server
    server = socket(AF_INET,SOCK_STREAM)
    
    #Creating Socket and wait for connection from client
    print (' Wainting For Connection...')
    s = socket(AF_INET,SOCK_STREAM)
    s.bind(("",6666))
    s.listen(5)
    client,a = s.accept()

    #Message Authentication Code/ HandShake / Negotiation Client
    MAC_LOCAL = bytes(random.randrange(1,pow(2,128)))
    keyClient = negotiate_key(client)
    MAC_HOST = client.recv(1024)

    #Extracting Server Name
    LogFile.write(MAC_HOST)
    #Extracting Server Name End
    
    client.sendall(encrypt(MAC_LOCAL,keyClient))
    MAC_HOST = decrypt(MAC_HOST,keyClient)
    MAC_RESPONSE = decrypt(client.recv(1024),keyClient)
    client.sendall(encrypt(MAC_HOST,keyClient))
		
    if MAC_RESPONSE != MAC_LOCAL:
        print(" BAD KEY NEGOTIATION WITH CLIENT... EXITING APPLICATION\n")
	sys.exit()
    else:
	print(" Negotiation with Client Successful")

    #Message Authentication Code/ HandShake / Negotiation Server
    serveraddress = 'localhost'
    server.connect((serveraddress,8011))
    MAC_LOCAL = bytes(random.randrange(1,pow(2,128)))
    keyServer = negotiate_key(server,'client')
    server.sendall(encrypt(MAC_LOCAL,keyServer))
    MAC_HOST = server.recv(1024)
    MAC_HOST = decrypt(MAC_HOST,keyServer)
    server.sendall(encrypt(MAC_HOST,keyServer))
    MAC_RESPONSE = decrypt(server.recv(1024),keyServer)
    if MAC_RESPONSE != MAC_LOCAL:
        print(" BAD KEY NEGOTIATION WITH SERVER... EXITING APPLICATION\n")
        sys.exit()
    else:
        print(" Negotiation with Server Successful\n")

    #User Name Exchange
    usernameclient = client.recv(1024)
    server.sendall(usernameclient)
    usernameserver = server.recv(1024)
    client.sendall(usernameserver)

    #Preparing Log File
    LogFile.write('\n\nCHAT LOG CAPTURED ON: ')
    LogFile.write(time.strftime('%d/%m/%Y') + ' at ' + time.strftime('%H:%M:%S') + '\n')
    LogFile.write('Participants: \n')
    LogFile.write('\tServer: ' + usernameserver + ' at ' + serveraddress + '\n')
    LogFile.write('\tClient: ' + usernameclient + ' at ' + a[0] + '\n\n')
    
    #Prepare Threads and Q
    q = Queue.Queue()

    listen_worker_client = threading.Thread(target=listen, args=(q,client,usernameclient,keyClient))
    listen_worker_client.setDaemon(True)
    listen_worker_client.start()

    listen_worker_server = threading.Thread(target=listen, args=(q,server,usernameserver,keyServer))
    listen_worker_server.setDaemon(True)
    listen_worker_server.start()

    # CHAT
    while True:
        if not q.empty():
            data = q.get()
            if data[0] == usernameclient:
                if data[1] == ':exit:\n':
                    print(usernameclient + ' closed connection (client)')
                    server.sendall(encrypt(data[1],keyServer))
                    LogFile.write(usernameclient + ' closed connection (client)')
                    LogFile.close()
                    q.task_done()
                    sys.exit()

                else:
                    prompt(usernameclient,data[1],LogFile)
                    server.sendall(encrypt(data[1],keyServer))
                    q.task_done()

            elif data[0] == usernameserver:
                if data[1] == ':exit:\n':
                    print(usernameserver + ' closed connection (server)')
                    client.sendall(encrypt(data[1],keyClient))
                    LogFile.write(usernameserver + ' closed connection (server)')
                    LogFile.close()
                    q.task_done()
                    sys.exit()

                else:
                    prompt(usernameserver,data[1],LogFile)
                    client.sendall(encrypt(data[1],keyClient))
                    q.task_done()
