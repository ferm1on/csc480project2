Both the client and the server program works from the same file

To run the client type
	python narcanus.py
and chose client mode

To run the server type
	python narcanus.py
and chose server mode

In both cases just enter the information you will be prompted with
and you are good to go.

To Exit the client/server once you start chating type :exit:

ATTENTION: Make sure you start the server first or the connection the
client will attemp to make will fail

narcanus server uses port 8011. make sure you router's firewall
as well as your OS's firewall is configure to allow connections there 